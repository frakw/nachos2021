// exception.cc 
//	Entry point into the Nachos kernel from user programs.
//	There are two kinds of things that can cause control to
//	transfer back to here from user code:
//
//	syscall -- The user code explicitly requests to call a procedure
//	in the Nachos kernel.  Right now, the only function we support is
//	"Halt".
//
//	exceptions -- The user code does something that the CPU can't handle.
//	For instance, accessing memory that doesn't exist, arithmetic errors,
//	etc.  
//
//	Interrupts (which can also cause control to transfer from user
//	code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1996 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "main.h"
#include "syscall.h"
#include <string>
#include <sstream>
#include <fstream>


//----------------------------------------------------------------------
// ExceptionHandler
// 	Entry point into the Nachos kernel.  Called when a user program
//	is executing, and either does a syscall, or generates an addressing
//	or arithmetic exception.
//
// 	For system calls, the following is the calling convention:
//
// 	system call code -- r2
//		arg1 -- r4
//		arg2 -- r5
//		arg3 -- r6
//		arg4 -- r7
//
//	The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//	"which" is the kind of exception.  The list of possible exceptions 
//	are in machine.h.
//----------------------------------------------------------------------

void
ExceptionHandler(ExceptionType which)
{
	int	type = kernel->machine->ReadRegister(2);
	int	val;
    switch (which) {
	case SyscallException:
	    switch(type) {
		case SC_Halt:
		{
		    DEBUG(dbgAddr, "Shutdown, initiated by user program.\n");
   		    kernel->interrupt->Halt();
		    break;
		}
		case SC_PrintInt:
		{
			val=kernel->machine->ReadRegister(4);
			cout << "Print integer:" <<val << endl;
			return;
		}
/*		case SC_Exec:
		{
			DEBUG(dbgAddr, "Exec\n");
			val = kernel->machine->ReadRegister(4);
			kernel->StringCopy(tmpStr, retVal, 1024);
			cout << "Exec: " << val << endl;
			val = kernel->Exec(val);
			kernel->machine->WriteRegister(2, val);
			return;
		}
*/		case SC_Exit:
		{
			DEBUG(dbgAddr, "Program exit\n");
			val=kernel->machine->ReadRegister(4);
			cout << "return value:" << val << endl;
			kernel->currentThread->Finish();
			break;
		}
		case SC_ThreadYield:
		{
			val=kernel->machine->ReadRegister(4);
			cout << "Call ThreadYield" << endl;
			kernel->currentThread->Yield();
			return;
		}
		case SC_Example:
		{
			val=kernel->machine->ReadRegister(4);
			cout << "Example value : "<< val << endl;
			return;
		}
		case SC_Log:
		{
			val=kernel->machine->ReadRegister(4);
			char input = val;
			//cout << "Log : " << input <<endl;
			string student_id = "B10815057";
			stringstream ss(student_id.substr(student_id.length()-2,2));
			int last2num;
			ss >> last2num;
			char lower_err_char = 'a' + last2num % 26;
			char upper_err_char = lower_err_char - 32;			
			ofstream output;
			output.open("NachOS.log",std::ios_base::app);
			output << "[" << student_id << "_Log]";
			if((input != lower_err_char) && (input != upper_err_char)){
				output << input;
			}
			else{
				output << "error";			
			}
			output << endl;
			output.close();
			return;
		}
		default:
		{
		    cerr << "Unexpected system call " << type << "\n";
 		    break;
		}
	    }
	    break;
	case PageFaultException:
		/*    Page Fault Exception    */
	    break;
	default:
	    cerr << "Unexpected user mode exception" << which << "\n";
	    break;
    }
    ASSERTNOTREACHED();
}
